const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);


const closeStatus = ["ANSWERED", "FAILED", "BUSY", "NO ANSWER"];
let id = 0;
let callHistory = [];
var usersOnChatCount = 0;

const config = {
    url: 'https://uni-call.fcc-online.pl',
    login: '',
    password: ''
};
const PORT = process.env.PORT || 3000;

Dialer.configure(config);
app.use(cors());
app.use(bodyParser.json());

http.listen(PORT, () => {
    console.log('app listening on port', PORT);
});

io.on('connection', (socket) => {    
    socket.on('disconnect', () => {
        --usersOnChatCount;
        let msg = 'A user disconnected. Users left: ' + usersOnChatCount;
        console.log(msg);
	    io.emit('message', msg);
    })
    socket.on('message', (message) => {
        io.emit('message', message);
        console.log('Message sent:', message);
    })
    ++usersOnChatCount;
    let msg = 'A user connected. Currently on chat: ' + usersOnChatCount;
    io.emit('message', msg);
    console.log(msg);
});

app.get('/status/:id', async (req, res) => {
    let status = callHistory[req.params.id];
    if (!status) return res.json({ success: false, status: "NO SUCH ID" });
    res.json({ success: true, status: status });
});

app.post('/call/', async (req, res) => {
    const body = req.body;
    let status = null;
    const currId = id;
    ++id;
    let bridge;
    try {
        bridge = await Dialer.call(body.number1, body.number2);
    } catch (e) {
        console.log('Call id', currId, 'failed');
        return res.json({ success: false });
    }
    let interval = setInterval(async () => {
        let oldStatus = status;
        status = await bridge.getStatus();
        if (status != oldStatus) {
            callHistory[currId] = status;
            io.emit('status', 'connection id ' + currId + ': ' + status);
        }
        if (closeStatus.includes(status)) {
            console.log("clearing interval for id:", currId);
            clearInterval(interval);
        }
    }, 500);
    console.log('Call id', currId, 'succesfull');
    res.json({ id: currId, success: true });
});
