# Instrukcja

Aplikacja podzielona jest na dwie części: backend i frontend. 

Aplikacja wymaga zainstalowanych:

- node.js 10.x 

- npm 

- pm2

## Pobranie z repozytorium

Aby pobrać aplikację z repozytorium otwieramy terminal i używamy polecenia `git clone https://astrevane@bitbucket.org/astrevane/focus-projekt-03.git`. 
Polecenie automatycznie utworzy folder o nazwie `focus-projekt-03` i umieści w nim pliki projektu, przechodzimy do tego folderu poleceniem `cd focus-projekt-03`. 
W folderze `focus-projekt-03` znajdują się foldery: 
`frontend` - zawierający pliki frontendu, oraz 
`backend` - zawierający pliki backendu. 

## Pobranie niezbędnych modułów

Należy pobrać wymagane moduły dla backendu i frontendu.
Będąc w folderze `focus-projekt-03`:

- pobieramy wymagane moduły dla backendu poleceniem `npm i --prefix ./backend/ ./backend/`. 

- pobieramy wymagane moduły dla frontendu poleceniem `npm i --prefix ./frontend/ ./frontend/`. 

## Uruchomienie frontendu

Przechodzimy do folderu `frontend` poleceniem `cd frontend` i wykonujemy polecenie `ng serve`.

Otwieramy nowe okno terminalu w folderze `focus-projekt-03`.

## Uruchomienie backendu

Najpierw musimy otworzyć plik `dialer.js` w folderze `backend` i wprowadzić login oraz hasło pomiędzy puste cudzysłowy w liniach 17 i 18 (pola login i password w obiekcie config), aby mieć dostęp do wykonywania połączeń.

Backend możemy uruchomić na dwa sposoby:

1) Używając PM2: wykonujemy polecenie `sudo su` aby zyskać uprawnienia administratora (wymaga podania hasła). 
Będąc w folderze `focus-projekt-03` wykonujemy polecenie `pm2 start ./backend.config.js`.

2) Będąc w folderze `focus-projekt-03` wykonujemy polecenie `node ./backend/dialer.js`.

## Użytkowanie aplikacji

Aby skorzystać z aplikacji otwieramy przeglądarkę i przechodzimy na adres `http://localhost:4200/`. Następnie w polach znajdujących się na lewo od przycisku `Zadzwoń`, wpisujemy numery telefonów pomiędzy którymi ma zostać nawiązane połączenie i jeśli pola z numerami nie świecą się na czerwono, klikamy na przycisk `Zadzwoń` (w innym wypadku numery nie przechodzą walidacji). Poniżej pól z numerami wyświetlany jest aktualny status połączenia.

Możemy również skorzystać ze znajdującego się na stronie czatu. Wiadomości wyświetlane są w szarych dymkach. Aby wysłać wiadomość wpisujemy ją do pustego pola tekstowego nad przyciskiem `Wyślij` i klikamy w tenże przycisk.
